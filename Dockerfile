FROM nginx:latest
COPY dist/angular-tour-of-heroes/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf